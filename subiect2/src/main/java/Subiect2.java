import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Subiect2 {

    public static void main(String[] args) {


        new window();

    }

}

class window extends JFrame implements ActionListener {

    private JButton button1;
    public static TextArea textArea1;
    private TextField textField1;
    public static String numeFisier;

    public static String sir;

    private static Object obiectSincronizare=new Object();



    window(){


        init();


    }



    public void init(){


        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(400,400);
        this.setVisible(true);
        this.setLayout(null);

        button1=new JButton("click");//create button
        button1.setBounds(130,75,100, 20);


        button1.addActionListener(this);

        textArea1=new TextArea();
        textArea1.setBounds(50,150,250,150);

        textField1=new TextField();
        textField1.setBounds(100,25,160,20);



        this.add(button1);
        this.add(textArea1);
        this.add(textField1);


    }


    @Override
    public void actionPerformed(ActionEvent e) {

        if(e.getSource()==button1){

numeFisier=textField1.getText();

//sincronizare cu obiect
Runnable prelucrare=new prelucrareFisier(obiectSincronizare);
Thread thread_prelucrare= new Thread(prelucrare);

thread_prelucrare.start();


        }



    }





}


class prelucrareFisier implements Runnable{

    private Object object;
    private String sir;
    private int count=0;
    private int linia=1;


    prelucrareFisier(Object object){

        this.object=object;

    }

    public void prelucrare(){


        synchronized (object){

            try {
                File myObj = new File(window.numeFisier);
                Scanner myReader = new Scanner(myObj);
                while (myReader.hasNextLine()) {
                    //citim linie cu linie
                    String data = myReader.nextLine();
                    for (int i = 0; i < data.length(); i++) {
                        if (data.charAt(i) == 'x') {
                            count++;
                        }
                    }
                    window.sir+="linia:"+linia+":"+count+"\n";

window.textArea1.setText(window.sir);

                    linia++;
                    count=0;
                }
                myReader.close();
            } catch (FileNotFoundException r) {
                System.out.println("Nu s-a gasit fisierul");
                r.printStackTrace();
            }

        }


    }


    @Override
    public void run() {

            prelucrare();

    }


}


