public class Subiect1 {
    //diagrama
}


class B extends G{
private long t;
private E objectE;

public void x(){
    System.out.println("metoda x");
}
}

class G{

    public void i(){
        System.out.println("metoda i");
    }

}

class E implements I{
    private F objectF;//compozitie
    private H objectH;//agregare

    public void metG(int i){
        System.out.println("metoda metG");
    }

    E(H objectH){
        this.objectF=new F();//compozitie (daca obiectul F depinde de clasa E , daca clasa E este stearsa(sau un obiect al ei)
        //si obiectul clasei F este sters.

        this.objectH=objectH;//agregare( ne folosim de un obiect existent primit de exemplu ca paramentru prin constructor)
    }

}

class F{

}

class H{

}

interface I{

}

class D{

    //relatie de dependenta, ne folosim de clasa B(de metode etc..prin intermediul unui obiect de clasa B)

    D(B objectB){
        objectB.x();//ne folosim de clasa B (depindem de ea)
    }

}


